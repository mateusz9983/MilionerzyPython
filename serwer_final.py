#!/usr/bin/env python
import socket, sys, threading
import sqlite3
import random
import demjson


def recv_all_until(sockfd, crlf):
    data = ""
    while not data.endswith(crlf):
        data = data + sockfd.recv(1)
    return data

class Ranking(object):
    def __init__(self, id, wynik,adres,port):
        self.id = id
        self.wynik = wynik
        self.adres=adres
        self.port=port


    def aktualizacja(self,lista, id, wynik):
        for x in lista:
            if x.id == id:
                x.wynik = wynik
        return lista


    def sortowanie_listy(self,lista):
        for i in range(len(lista) - 1, 0, -1):
            for j in range(i):
                if lista[j].wynik < lista[j + 1].wynik:
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
        return lista


    def zwroc_ranking(self,lista):
        ranking = ""
        ranking += "Ranking graczy: \n"
        for x in lista:
            ranking += "gracz: " +" ("+str(x.adres)+"):"+str(x.port)+" id: "+ str(x.id) + " jego obecna wygrana: " + str(x.wynik) + "\n"
        return ranking

class ClientThread(threading.Thread):
    def __init__(self, connection, id, adres, port, server,baza_pytan,sock):
        threading.Thread.__init__(self)
        self.connection = connection
        self.id = id
        self.server = server
        self.adres=adres
        self.port=port
        self.baza_pytan=baza_pytan
        self.sock=sock

    def losuj(self,licznikPytan,listaPytan):
        tmp=random.randint(41,82)
        if licznikPytan == 0:
            listaPytan.append(tmp)
            licznikPytan+=1
            return tmp,licznikPytan,listaPytan
        start=0
        while start < licznikPytan:
            if listaPytan[start] == tmp:
                tmp=random.randint(41,82)
                start = 0
            else:
                start+=1
        licznikPytan+=1
        listaPytan.append(tmp)
        return tmp,licznikPytan,listaPytan

    def wygrana(self,a):
        ListaKwot=[]
        if a == 0:
            return "500"
        ListaKwot.append(500)
        ListaKwot.append(1000)
        ListaKwot.append(2000)
        ListaKwot.append(5000)
        ListaKwot.append(10000)
        ListaKwot.append(20000)
        ListaKwot.append(40000)
        ListaKwot.append(75000)
        ListaKwot.append(125000)
        ListaKwot.append(250000)
        ListaKwot.append(500000)
        ListaKwot.append(1000000)
        return str(ListaKwot[a])

    def znajdz_numer_listy(self,lista,id):
        i=0
        for x in lista:
            x=x.split('/')
            x=x[0]
            x=int(x)
            if x==id:
                return i
            i+=1

    def zwroc_pytanie_odpowiedz_poprawna(self,id,lista):
        linia=lista[id].split('/')
        pytanie=linia[1]
        odpowiedz=linia[2]
        poprawna=linia[3]
        return pytanie, odpowiedz, poprawna

    def czywiecejnizmaks(self, rozmiar):        #zakladmy maks 1024 bajtow
        if rozmiar > 1024:
            return True
        else:
            return False


    def run(self):
        try:
            #DOZWOLONE KOMENDY TO:
            #HELLO\K\K
            #POBIERZPYTANIE\K\K
            #POBIERZODPOWIEDZ\K\K
            #SPRAWDZODPOWIEDZ\K\K
            #PUBLICZNOSC\K\K
            #TELEFON\K\K
            #50/50\K\K
            #KONCZGRE\K\K
            #RANKING\K\K
            crlf="\K\K"
            rank=Ranking(0,0,0,0)
            liczbaPoprawnychWiadmosci =0
            poprawna=""
            listaPytan=[]
            licznikPytan=0
            while True:

                data = recv_all_until(self.connection,crlf)
                #data=self.connection.recv(1024)
                #print data
                #data = demjson.decode(data)
                print data
                if data[len(data)-4:len(data)] == "\K\K": #and self.czywiecejnizmaks(data) == False:
                    #tmp=len('\K\K')
                    if data == "HELLO\K\K": #and len(data[0])+len(data[1]) == data[2]:
                        #paczka=demjson.encode(['DOZWOLONE KOMENDY TO: 1.Pobieranie Pytania, 2.Wycofuje sie, 3.Ranking graczy','\K\K',len('DOZWOLONE KOMENDY TO: 1.Pobieranie Pytania, 2.Wycofuje sie, 3.Ranking graczy')+len('\K\K')])
                        self.connection.sendall('DOZWOLONE KOMENDY TO: 1.Pobieranie Pytania, 2.Wycofuje sie, 3.Ranking graczy\K\K')
                    elif data == "POBIERZPYTANIE\K\K":
                        id,licznikPytan,listaPytan=self.losuj(licznikPytan,listaPytan)
                        print "Pytanie numer "+str(id)
                        recive="Grasz o: "+str(self.wygrana(liczbaPoprawnychWiadmosci))+"\n"
                        print recive
                        numer_listy=self.znajdz_numer_listy(self.baza_pytan,id)
                        pytanie,odpowiedz,poprawna=self.zwroc_pytanie_odpowiedz_poprawna(numer_listy,self.baza_pytan)
                        recive+=pytanie
                        recive+="    "
                        recive+=odpowiedz
                        recive+="\K\K"
                        #paczka=demjson.encode([recive,'\K\K',len(recive)+tmp])
                        self.connection.sendall(recive)
                        print 'Client ID %s Adres: %s Port: %s sent: ' % (self.id,(self.adres),(self.port)) +str(data)
                    elif data == "SPRAWDZODPOWIEDZ\K\K": #" #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        odp = recv_all_until(self.connection,crlf) #sprawdzic czy poprawny komunikat
                        #odp = demjson.decode(odp)
                        print poprawna
                        if odp[0] == poprawna: #and odp[len(odp)-4:len(odp)] == "\K\K": #and len(odp[0])+len(odp[1]) == odp[2] and self.czywiecejnizmaks(odp[2]) == False:
                            if liczbaPoprawnychWiadmosci == 11:
                                print 'Client ID %s disconnect' % (self.id)
                                #paczka=demjson.encode(["Gratulacje ","Zostales Milionerem",'\K\K',len("Gratulacje ")+len("Zostales Milionerem")+tmp])
                                self.connection.sendall("Zostales Milionerem\K\K")
                                wynik = int(self.wygrana(liczbaPoprawnychWiadmosci))
                                self.server.list_client = rank.aktualizacja(self.server.list_client, self.id, wynik)
                                self.server.list_client = rank.sortowanie_listy(self.server.list_client)
                                self.connection.close()
                                break;
                            reciveODp='Poprawna ODP'+'\n'
                            wynik=int(self.wygrana(liczbaPoprawnychWiadmosci))
                            self.server.list_client=rank.aktualizacja(self.server.list_client,self.id,wynik)
                            self.server.list_client=rank.sortowanie_listy(self.server.list_client)
                            #paczka=demjson.encode([reciveODp,'Twoja obecna wygrana to '+str(self.wygrana(liczbaPoprawnychWiadmosci)),'\K\K',len(reciveODp)+len('Twoja obecna wygrana to '+str(self.wygrana(liczbaPoprawnychWiadmosci)))+tmp])
                            self.connection.sendall(str(reciveODp)+'Twoja obecna wygrana to '+str(self.wygrana(liczbaPoprawnychWiadmosci))+"\K\K")
                            liczbaPoprawnychWiadmosci += 1
                        else:
                            if liczbaPoprawnychWiadmosci < 2:
                                wynik = 0
                                self.server.list_client = rank.aktualizacja(self.server.list_client, self.id, wynik)
                                self.server.list_client = rank.sortowanie_listy(self.server.list_client)
                                #paczka=demjson.encode(["NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ","Twoja wygrana to : 0",'\K\K',len("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ")+len("Twoja wygrana to : 0")+tmp])
                                self.connection.sendall("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY Twoja wygrana to : 0\K\K")
                                self.connection.close()
                            elif (liczbaPoprawnychWiadmosci >= 2) and (liczbaPoprawnychWiadmosci < 7):
                                wynik = 1000
                                self.server.list_client = rank.aktualizacja(self.server.list_client, self.id, wynik)
                                self.server.list_client = rank.sortowanie_listy(self.server.list_client)
                                #paczka=demjson.encode(["NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ","Twoja wygrana to : 1000",'\K\K',len("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ")+len("Twoja wygrana to : 1000")+tmp])
                                self.connection.sendall("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY Twoja wygrana to : 1000 \K\K")
                                self.connection.close()
                            elif liczbaPoprawnychWiadmosci >= 7:
                                wynik = 40000
                                self.server.list_client = rank.aktualizacja(self.server.list_client, self.id, wynik)
                                self.server.list_client = rank.sortowanie_listy(self.server.list_client)
                                #paczka=demjson.encode(["NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ","Twoja wygrana to : 40 000",'\K\K'],len("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ")+len("Twoja wygrana to : 40 000")+tmp)
                                self.connection.sendall("NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY ","Twoja wygrana to : 40 000\K\K")
                                self.connection.close()
                        print 'Client ID %s Adres: %s Port: %s sent: ' % (self.id,(self.adres),(self.port)) +str(data)
                    elif data == "PUBLICZNOSC\K\K": #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        recive=str(poprawna)
                        #paczka=demjson.encode([recive,'\K\K',len(recive)+tmp])
                        self.connection.sendall(recive+"\K\K")
                    elif data == "TELEFON\K\K": #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        recive=str(poprawna)
                        #paczka=demjson.encode([recive,'\K\K',len(recive)+tmp])
                        self.connection.sendall(recive+"\K\K")
                    elif data == "50/50\K\K": #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        if poprawna == "a":
                            #paczka=demjson.encode(["Komputer losowo odrzucil odpowiedz 'b' i 'd'.",'\K\K',len("Komputer losowo odrzucil odpowiedz 'b' i 'd'.")+tmp])
                            self.connection.sendall("Komputer losowo odrzucil odpowiedz 'b' i 'd'.\K\K")
                        elif poprawna == "b":
                            #paczka=demjson.encode(["Komputer losowo odrzucil odpowiedz 'a' i 'c'.",'\K\K',len("Komputer losowo odrzucil odpowiedz 'a' i 'c'.")+tmp])
                            self.connection.sendall("Komputer losowo odrzucil odpowiedz 'a' i 'c'.\K\K")
                        elif poprawna == "c":
                            #paczka=demjson.encode(["Komputer losowo odrzucil odpowiedz 'b' i 'd'.",'\K\K',len("Komputer losowo odrzucil odpowiedz 'b' i 'd'.")+tmp])
                            self.connection.sendall("Komputer losowo odrzucil odpowiedz 'b' i 'd'.\K\K")
                        elif poprawna == "d":
                            #paczka=demjson.encode(["Komputer losowo odrzucil odpowiedz 'b' i 'c'.",'\K\K',len("Komputer losowo odrzucil odpowiedz 'b' i 'c'.")+tmp])
                            self.connection.sendall("Komputer losowo odrzucil odpowiedz 'b' i 'c'.\K\K")
                    elif data == "KONCZGRE\K\K": #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        if liczbaPoprawnychWiadmosci == 0:
                            #paczka=demjson.encode(["0",'\K\K',len("0")+tmp])
                            self.connection.sendall("0\K\K")
                            self.connection.close()
                        else:
                            recive = str(self.wygrana(liczbaPoprawnychWiadmosci-1))
                            #paczka=demjson.encode([recive,'\K\K',len(recive)+tmp])
                            self.connection.sendall(recive+"\K\K")
                            self.connection.close()
                    elif data == "RANKING\K\K": #and len(data[0])+len(data[1]) == data[2] and self.czywiecejnizmaks(data[2]) == False:
                        send_ranking=rank.zwroc_ranking(self.server.list_client)
                        #paczka=demjson.encode([send_ranking,'\K\K',len(send_ranking)+tmp])
                        self.connection.sendall(send_ranking+"\K\K")
                    else:
                        print "Nie istnieje taki komunikat!"
                        #paczka=demjson.encode(["Nie istnieje taki komunikat",'\K\K',len("Nie istnieje taki komunikat")+tmp])
                        self.connection.sendall("Nie istnieje taki komunikat\K\K")
                else:
                    #paczka=demjson.encode(["Bledny Komunikat",'\K\K',len("Bledny Komunikat")+tmp])
                    self.connection.sendall("Bledny Komunikat\K\K")
            self.sock.close()
        except socket.error, e:
            if e.errno == 10054:
                print "Klient %d zerwal polaczenie..." % self.id
            elif e.errno == 9:
                print "Klient %d rozlaczyl sie..." %self.id
                self.connection.close()
class Server:
    def __init__(self, ip, port):
        self.server_adress = (ip, port)
        self.list_client =[]

    def polacz_z_baza(self):
        connection = sqlite3.connect('zdbEliminacje.db')
        cursor = connection.cursor()
        connection.text_factory = lambda x: unicode(x, "utf-8", "ignore")
        try:
            cursor.execute("select id from Pytania")
            connection.commit()
            return connection
        except:
            return str(0)

    def pobierz_z_bazy(self):
        con = self.polacz_z_baza()
        if con == "0":
            print "Nie mozna polaczyc z baza"
        else:
            cursor = con.cursor()
        lista_pytan=[]
        for i in range(41,84):
            linia=""
            for row in cursor.execute("select id from Pytania WHERE id=(%d)" % (i)):
                row = str(row)
                row=row[1:len(row)-2]
                linia+=row+"/"
            for row in cursor.execute("select Tresc from Pytania WHERE id=(%d)" % (i)):
                row = str(row)
                row=row[3:len(row)-3]
                linia+=row+"/"
            for row in cursor.execute("select odpowiedzi from Pytania WHERE id=(%d)" % (i)):
                row = str(row)
                row=row[4:len(row)-3]
                linia+=row+"/"
            for row in cursor.execute("select poprawnakolejnosc from Pytania WHERE id=(%d)" % (i)):
                row = str(row)
                row=row[4:len(row)-5]
                linia+=row
            lista_pytan.append(linia)
        con.close()
        return lista_pytan

    def run(self):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(self.server_adress)
            sock.listen(2)
            print 'Starting up TCP server on %s port %s' % self.server_adress
            id = 0

            while True:
                id += 1
                connection, client_address = sock.accept()
                ipport = connection.getpeername()
                print ipport
                adres = str(ipport[0])
                pport = str(ipport[1])
                print 'Client ID %s connected IP: %s Port: %s' %(id, adres , pport)
                lista=self.pobierz_z_bazy()
                c = ClientThread(connection, id,adres,pport, self,lista,sock)
                client= Ranking(id,0,adres,pport)
                self.list_client.append(client)
                c.start()

        except socket.error, e:
            if e.errno == 10048:
                print "Prawdopodobnie adres lub port jest zajety"
            else:
                print "Blad przy polaczeniu"


if __name__ == '__main__':
    s = Server('127.0.0.1', 6660)
    s.run()
