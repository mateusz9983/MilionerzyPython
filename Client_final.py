#!/usr/bin/env python
import socket
import sys
import re
adres = '127.0.0.1'
port = 6660
import demjson

def recv_all_until(sockfd, crlf):
    data = ""
    while not data.endswith(crlf):
        data = data + sockfd.recv(1)
    return data

class Client(object):
    def __init__(self):
        pass

    def sprawdzOdp(self,answer):
        if str(answer[len(answer)-4:len(answer)]) == "\K\K":
            return True
        else:
            return False
    def czywiecejnizmaks(self, rozmiar):        #zakladmy maks 1024 bajtow
        if rozmiar > 1024:
            return True
        else:
            return False

    def ip_valid(self,adres):
        try:
            socket.inet_aton(adres)
            return True
        except:
            return False

    def ip_valid_second_stage(self,adres):
        wzorzec = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        while True:
            test = wzorzec.search(adres)
            if test:
                adres = test.group()
                while (not self.ip_valid(adres)):
                    adres = raw_input("Niepoprawny adres serwera, podaj ponownie: ")
                break
            else:
                adres = raw_input("Niepoprawny adres serwera, podaj ponownie: ")
        return adres

    def client(self,HOST, PORT):
        server_address = (HOST, PORT)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        crlf="\K\K"
        try:
            sock.connect(server_address)
            try:
                #paczka=demjson.encode(["HELLO","\K\K",len('HELLO')+len('\K\K')])
                sock.sendall("HELLO\K\K")
                #print sock.recv(1024)
                answer = recv_all_until(sock,crlf)
                #print answer
                #answer =demjson.decode(answer)
                if self.sprawdzOdp(answer):# == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                    #print answer[2]
                    print answer[0:len(answer)-4]
                else:
                    print "Blad przy komunikacji z serverem"
                    sock.close()
                    sys.exit()
                publicznosc = True
                telefon = True
                polnapol = True
                while (answer[0:33] != "NIEPOPRAWNA ODPOWIEDZ, KONIEC GRY"):
                    komenda = raw_input("Podaj komende \n")
                    if komenda == "1":
                        #paczka=demjson.encode(["POBIERZPYTANIE","\K\K",len("POBIERZPYTANIE")+len("\K\K")])
                        #sock.sendall(paczka)
                        sock.sendall("POBIERZPYTANIE\K\K")
                        answer = recv_all_until(sock,crlf)
                        #answer =demjson.decode(answer)
                        if self.sprawdzOdp(answer):#  and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                            print answer[0:len(answer)-4]
                        else:
                            print "Blad przy komunikacji z serverem"
                            sock.close()
                            break
                        odpczykolo = ""
                        while odpczykolo != "o":
                            print "Odpowiedz(o), kolo raturnkowe(k), wyfocofaj sie(q) "
                            odpczykolo = raw_input()
                            if odpczykolo == "o":
                                #paczka=demjson.encode(["SPRAWDZODPOWIEDZ","\K\K",len("SPRAWDZODPOWIEDZ")+len("\K\K")])
                                sock.sendall("SPRAWDZODPOWIEDZ\K\K")
                                odp = raw_input("Podaj odpowiedz \n")  # teraz odbiera tu nie moza
                                #paczka=demjson.encode([str(odp),"\K\K",len(str(odp))+len("\K\K")])

                                sock.sendall(str(odp)+"\K\K")
                                answer=recv_all_until(sock,crlf)

                                #answer=demjson.decode(answer)
                                if self.sprawdzOdp(answer): #and len(answer[0])+len(answer[1]) +len(answer[2]) == answer[3] and self.czywiecejnizmaks(answer[3]) == False:
                                    if answer[0:len(answer)-4] == "Zostales Milionerem":
                                        print answer[0:len(answer)-4]
                                        sock.close()
                                        sys.exit()
                                    else:
                                        print answer[0:len(answer)-4]
                                        break
                                else:
                                    print "Blad przy komunikacji z serverem"
                                    sock.close()
                                    sys.exit()
                            elif odpczykolo == "k":
                                print "Jakie kolo chcesz wykorzystac: publicznosc(p), telefon (t), 50/50(h)"
                                jakiekolo=raw_input()
                                if jakiekolo == "p" and publicznosc == True:
                                    publicznosc=False
                                    #paczka=demjson.encode(["PUBLICZNOSC","\K\K",len("PUBLICZNOSC")+len("\K\K")])
                                    sock.sendall("PUBLICZNOSC\K\K")
                                    answer = recv_all_until(sock,crlf)
                                    #answer=demjson.decode(answer)
                                    if self.sprawdzOdp(answer): #and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                                        print "Publicznosc obstawia: " + answer[0:len(answer)-4]
                                        continue
                                    else:
                                        print "Blad przy komunikacji z serverem"
                                        sock.close()
                                        sys.exit()

                                elif jakiekolo == "t" and telefon == True:
                                    telefon=False
                                    #paczka=demjson.encode(["TELEFON","\K\K",len("TELEFON")+len("\K\K")])
                                    sock.sendall("TELEFON\K\K")
                                    answer = recv_all_until(sock,crlf)
                                    #answer=demjson.decode(answer)
                                    if self.sprawdzOdp(answer): #and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                                        print "HMMM mysle, ze to odpowiedz: " + answer[0:len(answer)-4]
                                        continue
                                    else:
                                        print "Blad przy komunikacji z serverem"
                                        sock.close()
                                        sys.exit()
                                elif jakiekolo == "h" and polnapol == True:
                                    polnapol=False
                                    #paczka=demjson.encode(["50/50","\K\K",len("50/50")+len("\K\K")])
                                    sock.sendall("50/50\K\K")
                                    answer = recv_all_until(sock,crlf)
                                    #answer=demjson.decode(answer)
                                    if self.sprawdzOdp(answer):# and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                                        print answer[0:len(answer)-4]
                                        continue
                                    else:
                                        print "Blad przy komunikacji z serverem"
                                        sock.close()
                                        sys.exit()
                                else:
                                    print "Bledny komunikat lub wykorzystane kolo"
                            elif odpczykolo == "q":
                                #paczka=demjson.encode(["KONCZGRE","\K\K",len("KONCZGRE")+len("\K\K")])
                                sock.sendall("KONCZGRE\K\K")
                                answer = recv_all_until(sock,crlf)
                                #answer=demjson.decode(answer)
                                if self.sprawdzOdp(answer): #and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                                    print "Twoja wygrana to: "+answer[0:len(answer)-4]
                                    sock.close()
                                    sys.exit()
                                else:
                                    print "Blad przy komunikacji z serverem"
                                    sock.close()
                                    sys.exit()
                    elif komenda == "2":
                        #paczka=demjson.encode(["KONCZGRE","\K\K",len("KONCZGRE")+len("\K\K")])
                        sock.sendall("KONCZGRE\K\K")
                        answer=recv_all_until(sock,crlf)
                        #answer=demjson.decode(answer)
                        if self.sprawdzOdp(answer):# and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                            print "Twoja wygrana to: " +answer
                            sock.close()
                            sys.exit()
                        else:
                            print "Blad przy komunikacji z serverem"
                            sock.close()
                    elif komenda == "3":
                        #paczka=demjson.encode(["RANKING","\K\K",len("RANKING")+len("\K\K")])
                        sock.sendall("RANKING\K\K")
                        answer = recv_all_until(sock,crlf)
                        #answer=demjson.decode(answer)
                        if self.sprawdzOdp(answer): #and len(answer[0])+len(answer[1]) == answer[2] and self.czywiecejnizmaks(answer[2]) == False:
                            print answer[0:len(answer)-4]
                        else:
                            print "Blad przy komunikacji z serverem"
                            sock.close()
                            sys.exit()
                    else:
                        print "Bledny komunikat"

            except socket.error, e:
                if e.errno == 10054:
                    print "Serwer zerwal polaczenie"
        except socket.error, e:
            if e.errno == 10061:
                print "Nie mozna polaczyc z serwerem, sprawdz ustawienia"
        sock.close()
if __name__=='__main__':
    c = Client()
    # adres = raw_input("Podaj adres serwera(127.0.0.1): ")
    # adres = c.ip_valid_second_stage(adres)
    # port = raw_input("Podaj port serwera: ")
    # while(len(port)==0 or port.isdigit()==False):
    #     port = raw_input("Port niedozwolony, podaj ponownie: ")
    # while(int(port) < 1025 or int(port) >65535):
    #     port=raw_input("Port niedozwolony, podaj ponownie: ")
    c.client(str(adres), int(port))
